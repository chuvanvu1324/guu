<?php if (isset($_POST['submit'])) : ?>
	<?php
		$firstName = $_POST['firstName'];
		$lastName = $_POST['lastName'];
		$adress = $_POST['adress'];
		$phone = $_POST['phone'];
		$email = $_POST['email'];
	?>
	<!DOCTYPE html>
	<html lang="en">
		<head>
			<meta charset="UTF-8">
			<title>Show info</title>
		</head>
		<body>
			<h1>Thông tin của bạn</h1>
			<ul>
				<li><?php echo "Họ Tên: " . $firstName . " " . $lastName; ?></li>
				<li><?php echo "Địa chỉ: " . $adress; ?></li>
				<li><?php echo "Số điện thoại: " . $phone; ?></li>
				<li><?php echo "Email: " . $email; ?></li>
			</ul>
		</body>
	</html>
<?php endif; ?>